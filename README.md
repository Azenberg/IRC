# Project
- IRC Server & Client

# Compilation

## Server
- gcc -c main.c -o main.o -lpthread
- gcc -c includes/user.c -o user.o -lpthread
- gcc main.o user.o -lpthread -o server.exe
- ./server.exe

## Client
- gcc client.c -o client.exe
- ./client.exe

# Author(s)
- Azedine Naidja
- Loic Goasguen
- André Morel
