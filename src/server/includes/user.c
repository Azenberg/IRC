#include <netdb.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "global.h"
#include "user.h"

#define max(x,y) ((x)>(y)?(x):(y))

void emptySocketBuffer(int socket){
	char c;
	int n = read(socket, &c, 1);
	
	while(n != 0){
		n = read(socket, &c, 1);
	}
}

int readSocket(int socket, char* dest, int length){
	char* pos;

	if (read(socket, dest, length) != -1){
		if ((pos = strchr(dest, '\n')) != NULL){
			*pos = '\0';
			return 0;
		}
		else{
			emptySocketBuffer(socket);
			return 1;
		}
	}else
		return -1;
}

User* createUser(int uSocket) {
	User *newUser = (User*) malloc(sizeof(User));
	strcpy(newUser->username, "Client1");
	newUser->uSocket = uSocket;
	newUser->currentRoom = NULL;
	pthread_mutex_init(&(newUser->uSocketMutex), NULL);
	
	return newUser;
}

UserList* createUserList(User *user) {
	UserList *result = (UserList*) malloc(sizeof(UserList));
	result->userNode = user;
	result->next = NULL;
	
	return result;
}

UserList* addToUserList(UserList *userList, User *user) {
	UserList *save = userList;
	
	if (!userList)
		return createUserList(user);
	
	while (userList->next) {
		userList = userList->next;
	}
	
	userList->next = createUserList(user);
	return save;
}

int userListLength(UserList *userList) {
	UserList *cur = userList;
	int length = 0;
	
	while (cur) {
		length++;
		cur = cur->next;
	}
	
	return length;
}

int sendMessage(Room *room, char *message, User *user) {
	if (room == NULL) {
		char *text = (char*) malloc((18 + strlen(message) + 1) * sizeof(char));
		bzero(text, strlen(text));
		strcpy(text, user->username);
		strcat(text, message);
		
		UserList *tmp = userListGlobal;
		while(tmp) {
			if(tmp->userNode != user){
				pthread_mutex_lock(&(tmp->userNode->uSocketMutex));
				write(tmp->userNode->uSocket, text, strlen(text) + 1);
				pthread_mutex_unlock(&(tmp->userNode->uSocketMutex));
			}
			
			tmp = tmp->next;
		}
		
		free(text);
	}
	
	return 0;
}

int setUsername(User *user, char *newUsername) {
	strcpy(user->username, newUsername);
	return 0;
}

int usernameAlreadyUsed() {
	return 1;
}

void startRoutine(User *user) {
	char welcomeMsg[100] = "************ Welcome to IRC Chat ! ************\n\n";
	char usernameMsg[100] = "[SERVER] Please choose a username (Max 15 char):";
	char usernameTooLong[100];
	char recv[100];
	fd_set readSet;
	int maxFd = max(user->uSocket, STDIN_FILENO);
	int n;
	
	int nameSetted = 0;
	
	strcat(welcomeMsg, usernameMsg);
	
	pthread_mutex_lock(&(user->uSocketMutex));
	write(user->uSocket, welcomeMsg, strlen(welcomeMsg) + 1);
	pthread_mutex_unlock(&(user->uSocketMutex));
	
	do {
		bzero(usernameMsg, 100);
		bzero(usernameTooLong, 100);
		strcpy(usernameMsg, "[SERVER] Please choose a username (Max 15 char): ");
		strcpy(usernameTooLong, "[SERVER] This username is too long !");
		
		bzero(recv, 100);
		FD_ZERO(&readSet);
		
		FD_SET(user->uSocket, &readSet);
		n = select((maxFd + 1), &readSet, NULL, NULL, NULL);
		
		if (n > 0){
			readSocket(user->uSocket, recv, 100);
			
			if (strlen(recv) > 15) {
				strcat(usernameTooLong, "\n");
				strcat(usernameTooLong, usernameMsg);
			
				pthread_mutex_lock(&(user->uSocketMutex));
				write(user->uSocket, usernameTooLong, strlen(usernameTooLong) + 1);
				pthread_mutex_unlock(&(user->uSocketMutex));
			} else {
				nameSetted = 1;
			}
		}
		
	} while (nameSetted == 0);
	
	
	printf("Sortie de la première boucle !\n");
	
	// Add the new user into the global list of users
	pthread_mutex_lock(&userListGlobalMutex);
	userListGlobal = addToUserList(userListGlobal, user);
	pthread_mutex_unlock(&userListGlobalMutex);

	while (1) {
		bzero(recv, 100);
		FD_ZERO(&readSet);
		
		FD_SET(user->uSocket, &readSet);
		n = select((maxFd + 1), &readSet, NULL, NULL, NULL);
		
		if (n > 0){
			readSocket(user->uSocket, recv, 100);
			sendMessage(NULL, recv, user);
		}
	}
}
