#ifndef USER_H
#define USER_H

#include "room.h"

// Structures
typedef struct user {
	char username[16];
	int uSocket;
	pthread_t uThread;
	Room* currentRoom;
	pthread_mutex_t uSocketMutex;
} User;

typedef struct userList {
	User* userNode;
	struct userList* next;
} UserList;


// Prototypes
User* createUser(int uSocket);
UserList* createUserList(User *u);
UserList* addToUserList(UserList *userList, User *user);
int userListLength(UserList *userList);
int setUsername(User *user, char *newUsername);
int usernameAlreadyUsed();

void startRoutine(User *user);

#endif
