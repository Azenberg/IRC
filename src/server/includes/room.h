#ifndef ROOM_H
#define ROOM_H

#include "user.h"

// Structures
typedef struct room {
	char* name;
	struct userList* roomUsers;
	pthread_mutex_t roomMutex;
} Room;

typedef struct roomList {
	Room* roomNode;
	struct roomList* next;
} RoomList;

#endif
