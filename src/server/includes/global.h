#ifndef GLOBAL_H
#define GLOBAL_H

#include "user.h"
#include "room.h"

// Global variables
extern UserList *userListGlobal;
extern pthread_mutex_t userListGlobalMutex;
extern RoomList *roomListGlobal;
extern pthread_mutex_t roomListGlobalMutex;

#endif
