#include <netdb.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "includes/global.h"
#include "includes/user.h"
#include "includes/room.h"

#include <arpa/inet.h>

#include <sys/types.h>
#include <sys/socket.h>


UserList *userListGlobal = NULL;
pthread_mutex_t userListGlobalMutex = PTHREAD_MUTEX_INITIALIZER;
RoomList *roomListGlobal = NULL;
pthread_mutex_t roomListGlobalMutex = PTHREAD_MUTEX_INITIALIZER;


int main(int argc, char **argv){
	int passive = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in servAddr;
	struct sockaddr_in clientAddr;
	socklen_t clientAddrSize = sizeof(clientAddr);
	char cliAddr[20];
	
	bzero(&servAddr, sizeof(servAddr));
	
	servAddr.sin_family = AF_INET;
	servAddr.sin_port = htons(8888);
	servAddr.sin_addr.s_addr = htons(INADDR_ANY);
	
	bind(passive, (struct sockaddr *) &servAddr, sizeof(servAddr));
	listen(passive, 10);
	
	system("clear");
	printf("[~] Server started on port 8888\n");
	
	User *newUser;
	while(1){
		// Waiting for a new connection
		int con = accept(passive, (struct sockaddr *) &clientAddr, &clientAddrSize);
		
		// Get the user adress and port
		inet_ntop(AF_INET, &(clientAddr.sin_addr.s_addr), cliAddr, clientAddrSize);
		printf("[~] Connection incoming from %s\n", cliAddr);
		
		// Creation of the user
		newUser = createUser(con);
		
		// Creation of a specific thread for the new user
		pthread_create(&(newUser->uThread), NULL, (void * (*) (void *)) startRoutine, newUser);
	}
	
	return EXIT_SUCCESS;
}
