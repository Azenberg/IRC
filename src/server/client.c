#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <arpa/inet.h>

#include <sys/types.h>
#include <sys/socket.h>

#define max(x,y) ((x)>(y)?(x):(y))

void emptyBuffer(){
	char c;

	while(c != '\n' && c != EOF){
		c = getchar();
	}
}


unsigned int lire(char* string, int length){
	char* pos;

	if (fgets(string, length, stdin) != NULL){
		if ((pos = strchr(string, '\n')) != NULL){
			*pos = '\0';
		}
		else{
			emptyBuffer();
		}

		return 1;
	}else
		return 0;
}

int main(int argc, char **argv){
	char send[100];
	char recv[100];
	int client = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in servAddr;
	
	bzero(&servAddr, sizeof(servAddr));
	
	servAddr.sin_family = AF_INET;
	servAddr.sin_port = htons(8888);
	inet_pton(AF_INET, "127.0.0.1", &(servAddr.sin_addr));
	
	connect(client, (struct sockaddr *) &servAddr, sizeof(servAddr));
	
	fd_set readSet;
	int maxFd = max(client, STDIN_FILENO);
	int n;
	
	while (1) {
		bzero(recv, 100);
		FD_ZERO(&readSet);
		
		/*struct timeval tv;
		tv.tv_sec = 1;
		tv.tv_usec = 500000;*/
		
		FD_SET(client, &readSet);
		n = select((maxFd + 1), &readSet, NULL, NULL, NULL);
		
		if(n < 0)
			printf("Error !");
			
		if(FD_ISSET(sock, readSet)){
			printf("Ok\n");
		}
		else{
			printf("NOK\n");
		}
	}
	
	return EXIT_SUCCESS;
}
